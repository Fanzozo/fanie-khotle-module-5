import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './add.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      // Remove the debug banner
      debugShowCheckedModeBanner: false, 
        
/*appBarTheme: const AppBarTheme(
              backgroundColor: Colors.deepPurple,
              // This will be applied to the "back" icon
              iconTheme: IconThemeData(color: Colors.red),
              // This will be applied to the action icon buttons that locates on the right side
              actionsIconTheme: IconThemeData(color: Colors.amber),
              centerTitle: false,
              elevation: 15,
              titleTextStyle: TextStyle(color: Colors.lightBlueAccent))
              ),*/
          
        
      title: 'module 5 App',
      home: HomePage(),
    );
  }
}
